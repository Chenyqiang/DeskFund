﻿namespace DeskFund
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Fundcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Jzrq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dwjz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gsz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gszzl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gztime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 10000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Fundcode,
            this._Name,
            this.Jzrq,
            this.Dwjz,
            this.Gsz,
            this.Gszzl,
            this.Gztime});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(430, 240);
            this.dataGridView1.TabIndex = 1;
            // 
            // Fundcode
            // 
            this.Fundcode.HeaderText = "CODE";
            this.Fundcode.Name = "Fundcode";
            this.Fundcode.ReadOnly = true;
            this.Fundcode.Visible = false;
            // 
            // _Name
            // 
            this._Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._Name.HeaderText = "名称";
            this._Name.Name = "_Name";
            this._Name.ReadOnly = true;
            // 
            // Jzrq
            // 
            this.Jzrq.HeaderText = "净值日期";
            this.Jzrq.Name = "Jzrq";
            this.Jzrq.ReadOnly = true;
            this.Jzrq.Visible = false;
            // 
            // Dwjz
            // 
            this.Dwjz.HeaderText = "单位净值";
            this.Dwjz.Name = "Dwjz";
            this.Dwjz.ReadOnly = true;
            this.Dwjz.Width = 80;
            // 
            // Gsz
            // 
            this.Gsz.HeaderText = "估算净值";
            this.Gsz.Name = "Gsz";
            this.Gsz.ReadOnly = true;
            this.Gsz.Width = 80;
            // 
            // Gszzl
            // 
            this.Gszzl.HeaderText = "估算涨跌";
            this.Gszzl.Name = "Gszzl";
            this.Gszzl.ReadOnly = true;
            this.Gszzl.Width = 80;
            // 
            // Gztime
            // 
            this.Gztime.HeaderText = "估算时间";
            this.Gztime.Name = "Gztime";
            this.Gztime.ReadOnly = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 240);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DeskFund";
            this.LocationChanged += new System.EventHandler(this.MainFormLocationChanged);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fundcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Jzrq;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dwjz;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gsz;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gszzl;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gztime;
    }
}