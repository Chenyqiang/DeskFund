﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeskFund.model
{
    [Serializable]
    class FundData
    {
        private string fundcode;

        public string Fundcode
        {
            get { return fundcode; }
            set { fundcode = value; }
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string jzrq;

        public string Jzrq
        {
            get { return jzrq; }
            set { jzrq = value; }
        }
        private string dwjz;

        public string Dwjz
        {
            get { return dwjz; }
            set { dwjz = value; }
        }
        private string gsz;

        public string Gsz
        {
            get { return gsz; }
            set { gsz = value; }
        }
        private string gszzl;

        public string Gszzl
        {
            get { return gszzl; }
            set { gszzl = value; }
        }
        private string gztime;

        public string Gztime
        {
            get { return gztime; }
            set { gztime = value; }
        }

        public FundData() { }

        public FundData(string _fundcode, string _name, string _jzrq, string _dwjz, string _gsz, string _gszzl, string _gztime)
        {
            this.fundcode = _fundcode;
            this.name = _name;
            this.jzrq = _jzrq;
            this.dwjz = _dwjz;
            this.gsz = _gsz;
            this.gszzl = _gszzl;
            this.gztime = _gztime;
        }

        public override string ToString()
        {
            return this.name;
        }
    }
}
