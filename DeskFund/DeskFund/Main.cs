﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using DeskFund.model;
using System.Threading;

namespace DeskFund
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            this.Text = this.Text + "    " + DateTime.Now.ToString("yyyy-MM-dd");
            getFundData();
            this.timer1.Enabled = true;
            this.timer2.Interval = ConvertUtil.ToInt32(IniHelper.ReadValue("Interval"),10000);
            this.timer2.Enabled = true;
        }

        #region 窗体隐藏部分
        void Timer1Tick(object sender, EventArgs e)
        {
            if (this.Bounds.Contains(Cursor.Position))
            {
                switch (this.StopAnhor)
                {
                    case AnchorStyles.Top:
                        this.Location = new Point(this.Location.X, 0);
                        break;
                    case AnchorStyles.Left:
                        this.Location = new Point(0, this.Location.Y);
                        break;
                    case AnchorStyles.Right:
                        this.Location = new Point(Screen.PrimaryScreen.Bounds.Width - this.Width, this.Location.Y);
                        break;
                }
            }
            else
            {
                switch (this.StopAnhor)
                {
                    case AnchorStyles.Top:
                        this.Location = new Point(this.Location.X, (this.Height - 4) * (-1));
                        break;
                    case AnchorStyles.Left:
                        this.Location = new Point((this.Width - 4) * (-1), this.Location.Y);
                        break;
                    case AnchorStyles.Right:
                        this.Location = new Point(Screen.PrimaryScreen.Bounds.Width - 4, this.Location.Y);
                        break;
                }
            }
        }

        internal AnchorStyles StopAnhor = AnchorStyles.None;
        private void mStopAnhor()
        {
            if (this.Top <= 0)
            {
                StopAnhor = AnchorStyles.Top;
            }
            else if (this.Left <= 0)
            {
                StopAnhor = AnchorStyles.Left;
            }
            else if (this.Right >= Screen.PrimaryScreen.Bounds.Width)
            {
                StopAnhor = AnchorStyles.Right;
            }
            else
            {
                StopAnhor = AnchorStyles.None;
            }
        }

        void MainFormLocationChanged(object sender, EventArgs e)
        {
            this.mStopAnhor();
        }
        #endregion

        /// <summary>
        /// 获取基金数据并显示
        /// </summary>
        public void getFundData() 
        {
            string url = IniHelper.ReadValue("Config","URL");
            string fundListStr = IniHelper.ReadValue("Config","FundCode");
            ArrayList fundList = new ArrayList(fundListStr.Split(','));
            if (fundList.Count > 0)
            {
                dataGridView1.Rows.Clear();
                foreach (string id in fundList)
                {
                    //获取json数据
                    RequestMessage requestMessage = RequestHelper.RequestGet(url.Replace("*", id));
                    if (requestMessage.Status == System.Net.HttpStatusCode.OK)
                    {
                        string json = requestMessage.Message;
                        json = json.Replace("jsonpgz(", "");
                        json = json.Replace(");", "");
                        //解析json数据
                        FundData fundData = JsonHelper.JsonDeserializeBySingleData<FundData>(json);
                        DateTime t = ConvertUtil.ToDateTime(fundData.Gztime, DateTime.Now);
                        //放入table
                        object[] d = {
                                         fundData.Fundcode,
                                         fundData.Name,
                                         fundData.Jzrq,
                                         fundData.Dwjz,
                                         fundData.Gsz,
                                         fundData.Gszzl,
                                         t.ToString("HH:mm:ss")                                         
                                     };
                        int i = dataGridView1.Rows.Add(d);;
                        if (ConvertUtil.ToDecimal(fundData.Gszzl,0)>0)
                        {
                            dataGridView1.Rows[i].Cells["Gszzl"].Style.BackColor = Color.Red;
                        }

                        if (ConvertUtil.ToDecimal(fundData.Gszzl, 0) < 0)
                        {
                            dataGridView1.Rows[i].Cells["Gszzl"].Style.BackColor = Color.Green;
                        }

                    }
                }
            }
        }
        /// <summary>
        /// 启动定时器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer2_Tick(object sender, EventArgs e)
        {
            getFundData();
        }
    }
}